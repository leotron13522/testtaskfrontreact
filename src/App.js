import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import GroupsList from './components/GroupsList';
import UsersList from './components/UsersList';
import GroupCreateUpdate from './components/GroupCreateUpdate';
import UserCreateUpdate from './components/UserCreateUpdate';

function App() {
  return (
    <div className="App">

        <Router>
           <Navbar />

           <Switch>
              <Route path='/' exact component={ UsersList } />

              <Route path='/groups/' exact component={ GroupsList } />
              <Route path='/groups/create' exact component={ GroupCreateUpdate } />
              <Route path='/groups/:pk' component={ GroupCreateUpdate } />

              <Route path='/users/' exact component={ UsersList } />
              <Route path='/users/create' component={ UserCreateUpdate } />
              <Route path='/users/:pk' component={ UserCreateUpdate } />

           </Switch>
        </Router>
    </div>
  );
}

export default App;
