import React, { Component } from 'react';
import UsersService from './User';
import  GroupsService  from  './Group';

const usersService = new UsersService();
const group = new GroupsService();

class UserCreateUpdate extends Component {
    constructor(props) {
        super(props);
        this.state  = {
            group: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      componentDidMount(){
        const { match: { params } } = this.props;
        console.log("check params from response", params)

        var  self  =  this;
        group.getGroups().then(function (result) {
            console.log(result);
            self.setState({ group:  result })
        });

        if(params && params.pk)
        {
          usersService.getUser(params.pk).then((c)=>{
            this.refs.Name.value = c.name;
            this.refs.group.value = c.group;
          })
        }
      }

      handleCreate(){
      console.log("check create parametres before creation: ", this.refs.Name.value, " group: ", this.refs.group.value, " try ")
        usersService.createUser(
          {
            "name": this.refs.Name.value,
            "group": this.refs.group.value
        }
        ).then((result)=>{
          alert("User created.");
          window.location.replace("/");
        }).catch(()=>{
          alert('There was an error! Please re-check your form.');
        });
      }

      handleUpdate(id){
        usersService.updateUser(
          {
            "pk": id,
            "name": this.refs.Name.value,
            "group": this.refs.group.value
        }
        ).then((result)=>{
          console.log(result);
          alert("User updated.");
          window.location.replace("/");
        }).catch(()=>{
          alert('There was an error! Please re-check your form.');
        });
      }



      handleSubmit(event) {

        const { match: { params } } = this.props;
        console.log("answer PARAMS", params)
        if(params && params.pk){
          this.handleUpdate(params.pk);
        }
        else
        {
          this.handleCreate();
        }

        event.preventDefault();
      }

      render() {
        return (
          <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>
              Name:</label>
              <input className="form-control" type="text" ref='Name' />

            <label>
              Group:</label>
              <select ref='group'>
                <option > </option>
                {this.state.group.map( c  =>

                        <option value={ c.id }>{ c.name }</option>

                    )}
              </select>


            <input className="btn btn-primary" type="submit" value="Submit" />
            </div>
          </form>
        );
      }
}

export default UserCreateUpdate;