import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class GroupsService{

    constructor(){}


    getGroups() {
        const url = `${API_URL}/groups/`;
        return axios.get(url).then(response => response.data);
    }
    getGroupsByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getGroup(pk) {
        const url = `${API_URL}/groups/${pk}`;
        return axios.get(url).then(response => response.data);
    }
    deleteGroup(group){
        const url = `${API_URL}/groups/${group.pk}`;
        console.log(url);
        return axios.delete(url);
    }
    createGroup(group){
        const url = `${API_URL}/groups/`;
        return axios.post(url,group);
    }
    updateGroup(group){
        const url = `${API_URL}/groups/${group.pk}`;
        return axios.put(url,group);
    }
}