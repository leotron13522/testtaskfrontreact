import  React, { Component } from  'react';
import  UsersService  from  './User';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import moment from 'moment';
import './styles.css';

const  user  =  new  UsersService();

class  UsersList  extends  Component {

    constructor(props) {
        super(props);
        this.state  = {
            user: []
        };
        this.handleDelete  =  this.handleDelete.bind(this);
    }

    componentDidMount() {
        var  self  =  this;
        user.getUsers().then(function (result) {
            self.setState({ user:  result })
        });
    }
    handleDelete(e,pk){
        var  self  =  this;
        user.deleteUser({pk :  pk}).then(()=>{
            var  newArr  =  self.state.user.filter(function(obj) {
                return  obj.id  !==  pk;
            });

            self.setState({user:  newArr})
        });
    }

    render() {

        return (
            <div  className="users--list">
              <Link to="/users/create"><button type="button" class="add-btn">Add User</button></Link>
                <table  className="table">
                    <thead  key="thead">
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Group</th>
                            <th>Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.user.map( c  =>
                            <tr  key={c.id}>
                            <td>{c.id}  </td>
                            <td>{c.name}</td>
                            <td>{c.group_name}</td>
                            <td>{moment(c.datetime_created).format('MMM DD, YYYY')}</td>
                            <td>
                            <button  onClick={(e)=>  this.handleDelete(e,c.id) }>Delete</button>
                            <Link to={{ pathname: `/users/${c.id}` }}><button type="button">Edit</button></Link>
                            </td>
                        </tr>)}
                    </tbody>
                </table>
            </div>
            );
  }
}
export  default  UsersList;