import  React, { Component } from  'react';
import  GroupsService  from  './Group';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

const  group  =  new  GroupsService();

class  GroupsList  extends  Component {

    constructor(props) {
        super(props);
        this.state  = {
            group: [],
        };
        this.handleDelete  =  this.handleDelete.bind(this);
    }

    componentDidMount() {
        var  self  =  this;
        group.getGroups().then(function (result) {
            console.log(result);
            self.setState({ group:  result })
        });
    }

    handleDelete(e,pk){
        var  self  =  this;
        group.deleteGroup({pk :  pk}).then(()=>{
            var  newArr  =  self.state.group.filter(function(obj) {
                return  obj.id  !==  pk;
            });

            self.setState({group:  newArr})
        }).catch((e)=>{
          alert("Can't delete group!");
        });
    }

    render() {

        return (
            <div  className="groups--list">
              <Link to="/groups/create"><button type="button" class="add-btn">Add Group</button></Link>
                <table  className="table">
                <thead  key="thead">
                <tr>
                    <th>#</th>
                    <th>Group Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {this.state.group.map( c  =>
                    <tr  key={c.id}>
                    <td>{c.id}  </td>
                    <td>{c.name}</td>
                    <td>{c.description}</td>
                    <td>
                    <button  onClick={(e)=>  this.handleDelete(e,c.id) }> Delete</button>
                    <Link to={{ pathname: `/groups/${c.id}` }} ><button type="button">Edit</button></Link>
                    </td>
                </tr>)}
                </tbody>
                </table>
            </div>
            );
  }
}
export  default  GroupsList;